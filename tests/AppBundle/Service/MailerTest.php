<?php

namespace tests\Service;

use AppBundle\Service\Mailer;
use PHPUnit\Framework\TestCase;

class MailerTest extends TestCase
{
    public function testSendMail()
    {
        $swift = $this->createMock(\Swift_Mailer::class);

        $mailer = new Mailer($swift);

        $datas = [
            'subject' => 'subject',
            'from' => 'from@from.fr',
            'to' => 'to@to.fr',
            'body' => 'body',
            ];
        $message = $mailer->sendMail($datas);

        $this->assertEquals('subject', $message->getSubject());
        $this->assertEquals(['from@from.fr' => null], $message->getFrom());
        $this->assertEquals(['to@to.fr' => null], $message->getTo());
        $this->assertEquals('body', $message->getBody());
    }
}