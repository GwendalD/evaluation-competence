<?php

namespace tests\AppBundle\Service;

use AppBundle\Service\SerieSearcher;
use PHPUnit\Framework\TestCase;

class SerieSearcherTest extends TestCase
{
    public function testSearch()
    {
        $searcher = New SerieSearcher();

        $data = [
            [
                'title' => 'something',
                'aliases' => [
                    'test',
                    'another',
                ]

            ],
            [
                'title' => 'needle',
                'aliases' => [
                    'alias',
                ]
            ],
        ];

        $data2 = [
            [
                'title' => 'something',
                'aliases' => [
                    'needle',
                    'test',
                ]

            ],
            [
                'title' => 'title',
                'aliases' => [
                    'alias',
                ]
            ],
        ];

        $data3 = [
            [
                'title' => 'something',
                'aliases' => [
                    'alias',
                ]

            ],
            [
                'title' => 'title',
                'aliases' => [
                    'alias',
                ]
            ],
        ];

        $this->assertEquals($data[1], $searcher->search('needle', $data));
        $this->assertEquals($data2[0], $searcher->search('needle', $data2));
        $this->assertEquals(['Not found'], $searcher->search('needle', $data3));
    }
}