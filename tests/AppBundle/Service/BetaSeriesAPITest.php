<?php

namespace tests\Service;

use AppBundle\Entity\Serie;
use AppBundle\Exception\JsonDecodeException;
use AppBundle\Service\BetaSeriesAPI;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;

class BetaSeriesAPITest extends TestCase
{
    public function testGetSerieFromName()
    {
        $key = 'key';

        $client = $this->getClient(200, 'body');

        $api = new BetaSeriesAPI($key, $client);
        $response = $api->getSerieFromName('name');

        $this->assertEquals('body', $response['body']);
    }

    public function testGetSerieFromNameWithError()
    {
        $key = 'key';

        $client = $this->getClient(400);

        $api = new BetaSeriesAPI($key, $client);
        $response = $api->getSerieFromName('name');

        $this->assertEquals('400', $response['error']);
    }

    public function testGetSerieFromID()
    {
        $key = 'key';

        $client = $this->getClient(200, 'body');

        $api = new BetaSeriesAPI($key, $client);

        $serie = new Serie();
        $serie->setBetaId(1);

        $response = $api->getSerieFromID($serie);

        $this->assertEquals('body', $response['body']);
    }

    public function testGetSerieFromIDWithError()
    {
        $key = 'key';

        $client = $this->getClient(400);

        $api = new BetaSeriesAPI($key, $client);

        $serie = new Serie();
        $serie->setBetaId(1);

        $response = $api->getSerieFromID($serie);

        $this->assertEquals('400', $response['error']);
    }

    public function testGetEpisode()
    {
        $key = 'key';

        $client = $this->getClient(200, 'body');

        $api = new BetaSeriesAPI($key, $client);

        $serie = new Serie();
        $serie->setBetaId(1);

        $response = $api->getEpisode('s1e1', $serie);

        $this->assertEquals('body', $response['body']);
    }

    public function testGetEpisodeWithError()
    {
        $key = 'key';

        $client = $this->getClient(400);

        $api = new BetaSeriesAPI($key, $client);

        $serie = new Serie();
        $serie->setBetaId(1);

        $response = $api->getEpisode('s1e1', $serie);

        $this->assertEquals('400', $response['error']);
    }

    public function testJsonDecode()
    {
        $key = 'key';
        $client = $this->createMock(Client::class);

        $api = new BetaSeriesAPI($key, $client);

        $body = json_encode(['body' => 'body']);

        $output = $api->jsonDecode($body);

        $this->assertEquals('body', $output['body']);
    }

    public function testJsonDecodeWithError()
    {
        $this->expectException(JsonDecodeException::class);

        $key = 'key';
        $client = $this->createMock(Client::class);

        $api = new BetaSeriesAPI($key, $client);

        $api->jsonDecode('test');

    }

    private function getClient(int $status, string $body = null)
    {
        $mock = new MockHandler([
            ($body === null) ?
                new Response($status, ['X-Foo' => 'Bar'])
                :
                new Response($status, ['X-Foo' => 'Bar'], json_encode(['body' => $body]))
        ]);

        $handler = HandlerStack::create($mock);

        $client = new Client(['handler' => $handler]);

        return $client;
    }
}