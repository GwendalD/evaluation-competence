<?php

namespace tests\AppBundle\Service;

use AppBundle\Entity\Episode;
use AppBundle\Entity\Serie;
use AppBundle\Manager\EpisodeManager;
use AppBundle\Service\BetaSeriesAPI;
use AppBundle\Service\SerieSynchronizer;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class SerieSynchronizerTest extends TestCase
{
    public function testShiftArray()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $api = $this->createMock(BetaSeriesAPI::class);
        $manager = $this->createMock(EpisodeManager::class);

        $synchronizer = new SerieSynchronizer($em, $api, $manager);

        $serie = new Serie();
        $serie->setNbSeasons(1);

        $output = [
            'show' => [
                'seasons_details' => [
                    [
                        'number' => 1,
                        'episodes' => 10,
                    ],
                    [
                        'number' => 2,
                        'episodes' => 10,
                    ],
                ]
            ]

        ];

        $output = $synchronizer->shiftArray($serie, $output);

        $this->assertEquals(1, count($output['show']['seasons_details']));
    }

    public function testSyncSerie()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $manager = $this->createMock(EpisodeManager::class);

        $api = $this->getMockBuilder(BetaSeriesAPI::class)
            ->disableOriginalConstructor()
            ->getMock();

        $api->expects($this->once())
            ->method('getSerieFromID')
            ->willReturn([
                'show' => [
                    'seasons' => 2,
                    'seasons_details' => [
                        [
                            'number' => 1,
                            'episodes' => 1,
                        ],
                        [
                            'number' => 2,
                            'episodes' => 1,
                        ],
                    ]
                ]
            ]);

        $serie = new Serie();
        $serie->setNbSeasons(1);

        $synchronizer = new SerieSynchronizer($em, $api, $manager);

        $serie = $synchronizer->syncSerie($serie);

        $this->assertEquals(2, $serie->getNbSeasons());
    }

    public function testUpdateEpisodes()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $manager = $this->createMock(EpisodeManager::class);

        $api = $this->getMockBuilder(BetaSeriesAPI::class)
            ->disableOriginalConstructor()
            ->getMock();

        $api->expects($this->once())
            ->method('getEpisode')
            ->willReturn([
                'episode' => [
                    'description' => 'description'
                ]
            ]);

        $serie = new Serie();

        $episode = new Episode();
        $episode->setSeason(1)
            ->setNumber(1);

        $episodes = [$episode];

        $synchronizer = new SerieSynchronizer($em, $api, $manager);

        $synchronizer->updateEpisodes($serie, $episodes);

        $this->assertEquals('description', $episode->getSynopsys());

    }
}