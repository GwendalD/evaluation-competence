<?php

namespace tests\Service;

use AppBundle\Service\ImageConverter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageConverterTest extends TestCase
{
    public function testCreateImageFromString()
    {
        $converter = new ImageConverter();

        $file = $converter->downloadImage('https://symfony.com/images/v5/logos/header-logo.svg');

        $this->assertInstanceOf(UploadedFile::class, $file);
    }
}