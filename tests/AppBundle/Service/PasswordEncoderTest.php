<?php

namespace tests\Service;

use AppBundle\Entity\User;
use AppBundle\Service\PasswordEncoder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordEncoderTest extends TestCase
{
    public function testEncodePassword()
    {
        $encoderInterface = $this->getMockBuilder(UserPasswordEncoderInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $encoderInterface->expects($this->once())
            ->method('encodePassword')
            ->willReturn('test');

        $encoder = new PasswordEncoder($encoderInterface);

        $user = new User();
        $user->setPassword('test');

        $password = $encoder->encodePassword($user, $user->getPassword());

        $this->assertEquals('test', $password);
    }
}