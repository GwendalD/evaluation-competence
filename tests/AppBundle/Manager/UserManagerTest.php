<?php

namespace tests\AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Manager\UserManager;
use AppBundle\Service\PasswordEncoder;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class UserManagerTest extends TestCase
{

    public function testCreateUser()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $encoder = $this->getMockBuilder(PasswordEncoder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $encoder
            ->expects($this->once())
            ->method('encodePassword')
            ->willReturn('test');

        $userManager = new UserManager($encoder, $em);

        $user = new User();
        $user->setPassword('password');

        $user = $userManager->createUser($user);

        $this->assertEquals('test', $user->getPassword());
        $this->assertEquals(['ROLE_USER'], $user->getRoles());
        $this->assertEquals(true, $user->isEnabled());
    }

    public function testCreateUserFromCommand()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $encoder = $this->getMockBuilder(PasswordEncoder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $encoder
            ->expects($this->once())
            ->method('encodePassword')
            ->willReturn('test');

        $userManager = new UserManager($encoder, $em);

        $username = 'username';
        $email = 'test@test.fr';
        $password = 'pwd';

        $user = $userManager->createUserFromCommand($username, $email, $password);

        $this->assertEquals($username, $user->getUsername());
        $this->assertEquals($email, $user->getEmail());
        $this->assertEquals('test', $user->getPassword());
        $this->assertEquals(['ROLE_USER'], $user->getRoles());
        $this->assertEquals(true, $user->isEnabled());
    }

    public function testChangeRoleFromCommand()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $encoder = $this->createMock(PasswordEncoder::class);

        $userManager = new UserManager($encoder, $em);

        $user = new User();

        $user = $userManager->changeRoleFromCommand($user, 'ROLE_ADMIN');

        $this->assertEquals(['ROLE_ADMIN'], $user->getRoles());
    }

    public function testEditUser()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $encoder = $this->createMock(PasswordEncoder::class);

        $userManager = new UserManager($encoder, $em);

        $user = new User();

        $editedUser = $userManager->editUser($user);

        $this->assertEquals($user, $editedUser);
    }

    public function testEditPassword()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $encoder = $this->getMockBuilder(PasswordEncoder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $encoder
            ->expects($this->once())
            ->method('encodePassword')
            ->willReturn('test');

        $userManager = new UserManager($encoder, $em);

        $user = new User();
        $user->setPassword('test');

        $user = $userManager->editPassword($user);

        $this->assertEquals('test', $user->getPassword());
    }

}