<?php

namespace tests\AppBundle\Manager;

use AppBundle\Entity\Episode;
use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use AppBundle\Manager\DiscussionManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class DiscussionManagerTest extends TestCase
{
    public function testPostMessage()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $episode = new Episode();
        $episode->setTitle('Title');

        $user = new User();
        $user->setUsername('Username');

        $message = new Message();

        $discussionManager = new DiscussionManager($em);
        $discussionManager->postMessage($message, $episode, $user);

        $this->assertEquals($episode->getTitle(), $message->getEpisode()->getTitle());
        $this->assertEquals($user->getUsername(), $message->getUser()->getUsername());
    }

    public function testEditMessage()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $message = new Message();

        $discussionManager = new DiscussionManager($em);

        $this->assertEquals($message, $discussionManager->editMessage($message));

    }

    public function testDeleteMessage()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $message = new Message();

        $discussionManager = new DiscussionManager($em);

        $discussionManager->deleteMessage($message);

        $this->assertTrue(true);
    }
}