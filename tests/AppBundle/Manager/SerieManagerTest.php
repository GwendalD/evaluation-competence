<?php

namespace tests\AppBundle\Manager;

use AppBundle\Entity\Serie;
use AppBundle\Manager\EpisodeManager;
use AppBundle\Manager\SerieManager;
use AppBundle\Service\BetaSeriesAPI;
use AppBundle\Service\ImageConverter;
use AppBundle\Service\SerieSynchronizer;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\RouterInterface;

class SerieManagerTest extends TestCase
{
    public function testCreate()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $converter = $this->createMock(ImageConverter::class);
        $api = $this->createMock(BetaSeriesAPI::class);
        $manager = $this->createMock(EpisodeManager::class);
        $synchro = $this->createMock(SerieSynchronizer::class);
        $router = $this->createMock(RouterInterface::class);

        $serieManager = new SerieManager($em, $converter, $api, $manager,$synchro,$router);

        $datas = [
            'id' => 1,
            'title'=> 'title',
            'seasons' => 2,
            'description' => 'description',
            'images' => [
                'show' => 'test'
            ],
            'seasons_details' => [
                'number' => 1,
                'episodes' => 1,
            ],
        ];

        $serie = $serieManager->create($datas);

        $this->assertEquals($datas['id'], $serie->getBetaId());
        $this->assertEquals($datas['title'], $serie->getName());
        $this->assertEquals($datas['seasons'], $serie->getNbSeasons());
        $this->assertEquals($datas['description'], $serie->getSynopsys());
        $this->assertEquals(0, $serie->getConsulted());
    }
    
    public function testUpdateSerie()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $converter = $this->createMock(ImageConverter::class);
        $api = $this->createMock(BetaSeriesAPI::class);
        $manager = $this->createMock(EpisodeManager::class);
        $synchro = $this->createMock(SerieSynchronizer::class);
        $router = $this->createMock(RouterInterface::class);

        $serieManager = new SerieManager($em, $converter, $api, $manager,$synchro,$router);

        $serie = new Serie();
        $serie->setConsulted(0);

        $serieManager->updateSerie($serie);

        $this->assertEquals(1, $serie->getConsulted());
    }
}