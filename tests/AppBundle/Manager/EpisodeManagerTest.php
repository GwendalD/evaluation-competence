<?php

namespace tests\AppBundle\Manager;

use AppBundle\Entity\Serie;
use AppBundle\Manager\EpisodeManager;
use AppBundle\Service\BetaSeriesAPI;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class EpisodeManagerTest extends TestCase
{
    public function testgetEpisodesNumber()
    {
        $session = $this->createMock(SessionInterface::class);
        $api = $this->createMock(BetaSeriesAPI::class);
        $em = $this->createMock(EntityManagerInterface::class);

        $episodeManager = new EpisodeManager($session, $api, $em);

        $input = [
            [
                'number' => 1,
                'episodes' => 2,
            ],
            [
                'number' => 2,
                'episodes' => 1
            ]
        ];

        $numbers = $episodeManager->getEpisodesNumber($input);

        $this->assertEquals('s1e1', $numbers[0]);
        $this->assertEquals('s1e2', $numbers[1]);
        $this->assertEquals('s2e1', $numbers[2]);
    }

    public function testCreateEpisode()
    {
        $session = $this->createMock(SessionInterface::class);
        $api = $this->createMock(BetaSeriesAPI::class);
        $em = $this->createMock(EntityManagerInterface::class);

        $episodeManager = new EpisodeManager($session, $api, $em);

        $serie = new Serie();
        $serie->setName('name');

        $output = [
            'title' => 'title',
            'description' => 'description',
            'season' => 1,
            'episode' => 1,
        ];

        $episode = $episodeManager->createEpisode($serie, $output);

        $this->assertEquals($serie->getName() ,$episode->getSerie()->getName());
        $this->assertEquals($output['title'], $episode->getTitle());
        $this->assertEquals($output['description'], $episode->getSynopsys());
        $this->assertEquals($output['season'], $episode->getSeason());
        $this->assertEquals($output['episode'], $episode->getNumber());
    }

    public function testCreateEpisodesFromAPI()
    {
        $session = $this->createMock(SessionInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $api = $this->getMockBuilder(BetaSeriesAPI::class)
            ->disableOriginalConstructor()
            ->getMock();

        $api->expects($this->once())
            ->method('getEpisode')
            ->willReturn([
                'episode' => [
                    'title' => 'title',
                    'description' => 'description',
                    'season' => 1,
                    'episode' => 1,
                ],
                'errors' => [],
            ]);

        $seasonsDetails = [
            [
                'number' => 1,
                'episodes' => 1,
            ],
        ];

        $serie = new Serie();
        $serie->setName('name');

        $episodeManager = new EpisodeManager($session, $api, $em);

        $episodes = $episodeManager->createEpisodesFromAPI($serie, $seasonsDetails);

        $this->assertEquals('title', $episodes[0]->getTitle());
        $this->assertEquals('description', $episodes[0]->getSynopsys());
        $this->assertEquals('name', $episodes[0]->getSerie()->getName());
        $this->assertEquals(1, $episodes[0]->getSeason());
        $this->assertEquals(1, $episodes[0]->getNumber());
    }

    public function testCreateEpisodesFromAPIWithError()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $bag = $this->getMockBuilder(FlashBagInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $bag->expects($this->once())
            ->method('add');

        $session = $this->getMockBuilder(Session::class)
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->once())
            ->method('getFlashBag')
            ->willReturn($bag);


        $api = $this->getMockBuilder(BetaSeriesAPI::class)
            ->disableOriginalConstructor()
            ->getMock();

        $api->expects($this->once())
            ->method('getEpisode')
            ->willReturn([
                'episode' => [],
                'errors' => [
                    'error'
                ],
            ]);

        $seasonsDetails = [
            [
                'number' => 1,
                'episodes' => 1,
            ],
        ];

        $serie = new Serie();
        $serie->setName('name');

        $episodeManager = new EpisodeManager($session, $api, $em);

        $episodes = $episodeManager->createEpisodesFromAPI($serie, $seasonsDetails);

        $this->assertEquals(0, count($episodes));
    }
}