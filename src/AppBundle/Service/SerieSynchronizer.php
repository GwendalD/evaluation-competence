<?php

namespace AppBundle\Service;

use AppBundle\Entity\Episode;
use AppBundle\Entity\Serie;
use AppBundle\Manager\EpisodeManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SerieSynchronizer
 */
class SerieSynchronizer
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BetaSeriesAPI
     */
    private $betaSeriesAPI;

    /**
     * @var EpisodeManager
     */
    private $episodeManager;

    /**
     * SerieSynchronizer constructor.
     *
     * @param EntityManagerInterface $em
     * @param BetaSeriesAPI $betaSeriesAPI
     * @param EpisodeManager $episodeManager
     */
    public function __construct(EntityManagerInterface $em, BetaSeriesAPI $betaSeriesAPI, EpisodeManager $episodeManager)
    {
        $this->em = $em;
        $this->betaSeriesAPI = $betaSeriesAPI;
        $this->episodeManager = $episodeManager;
    }

    /**
     * Compare the serie in the database to the one in BetaSeries' and get it up to date
     *
     * @param Serie $serie
     *
     * @return Serie
     */
    public function syncSerie(Serie $serie): Serie
    {
        $output = $this->betaSeriesAPI->getSerieFromID($serie);

        if ($serie->getNbSeasons() !== $output['show']['seasons']) {
            $output = $this->shiftArray($serie, $output);

            $this->episodeManager->createEpisodesFromAPI($serie, $output['show']['seasons_details']);

            $serie->setNbSeasons($output['show']['seasons']);
            $serie->setUpdatedAt(new \DateTime());

            $this->em->persist($serie);
        }

        $this->em->flush();

        return $serie;
    }

    /**
     * @param Serie $serie
     * @param string[] $output
     *
     * @return string[]
     */
    public function shiftArray(Serie $serie, array $output): array
    {
        for ($i = 0; $i < $serie->getNbSeasons(); $i++) {
            array_shift($output['show']['seasons_details']);
        }

        return $output;
    }

    /**
     * Update episodes that now have an synopsys in BetaSeries' database
     *
     * @param Serie $serie
     * @param Episode[] $episodes
     */
    public function updateEpisodes(Serie $serie, array $episodes): void
    {
        foreach ($episodes as $episode) {
            if (strlen($episode->getSynopsys()) === 0) {
                $number = 's'.$episode->getSeason().'e'.$episode->getNumber();

                $output = $this->betaSeriesAPI->getEpisode($number, $serie);
                if (strlen($output['episode']['description']) !== 0) {
                    $episode->setSynopsys($output['episode']['description']);
                    $this->em->persist($episode);
                }
            }
        }
    }
}
