<?php

namespace AppBundle\Service;

use AppBundle\Entity\Serie;
use AppBundle\Exception\JsonDecodeException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class BetaSeriesAPI
 */
class BetaSeriesAPI
{
    /**
     * @var $key
     */
    private $key;

    /**
     * @var Client
     */
    private $betaClient;

    /**
     * BetaSeriesAPI constructor.
     *
     * @param $betaseriesKey
     * @param Client $betaClient
     */
    public function __construct($betaseriesKey, Client $betaClient)
    {
        $this->key = $betaseriesKey;
        $this->betaClient = $betaClient;
    }

    /**
     * @param string $name
     *
     * @return string[]
     */
    public function getSerieFromName(string $name): array
    {
        $serieName = str_replace(' ', '+', $name);

        try {
            $response = $this->betaClient->request('GET', 'shows/search', [
                'query' => [
                    'title' => $serieName,
                    'key' => $this->key,
                ]
            ]);

            $output = $this->jsonDecode($response->getBody());

            return $output;
        } catch (GuzzleException $e) {
            return ['error' => $e->getCode()];
        }
    }

    /**
     * @param Serie $serie
     *
     * @return string[]
     */
    public function getSerieFromID(Serie $serie): array
    {
        try {
            $response = $this->betaClient->request('GET', 'shows/display', [
                'query' => [
                    'id' => $serie->getBetaId(),
                    'key' => $this->key,
                ]
            ]);

            $output = $this->jsonDecode($response->getBody());

            return $output;
        } catch (GuzzleException $e) {
            return ['error' => $e->getCode()];
        }
    }

    /**
     * @param string $number
     * @param Serie $serie
     *
     * @return string[]
     */
    public function getEpisode(string $number, Serie $serie): array
    {
        try {
            $response = $this->betaClient->request('GET', 'episodes/search', [
                'query' => [
                    'show_id' => $serie->getBetaId(),
                    'number' => $number,
                    'key' => $this->key,
                ]
            ]);

            $output = $this->jsonDecode($response->getBody());

            return $output;
        } catch (GuzzleException $e) {
            return ['error' => $e->getCode()];
        }
    }

    /**
     * @param string $body
     *
     * @return string[]
     */
    public function jsonDecode(string $body): array
    {
        $output = json_decode($body, true);

        if ($output !== null) {
            return $output;
        }

        throw new JsonDecodeException("Couln't decode, bad Json");
    }
}
