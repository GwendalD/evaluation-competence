<?php

namespace AppBundle\Service;

/**
 * Class Mailer
 */
class Mailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * Mailer constructor.
     *
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string[] $datas
     *
     * @return \Swift_Message
     */
    public function sendMail(array $datas): \Swift_Message
    {
        $message = (new \Swift_Message($datas['subject']))
            ->setFrom($datas['from'])
            ->setTo($datas['to'])
            ->setBody($datas['body']);

        $this->mailer->send($message);

        return $message;
    }
}
