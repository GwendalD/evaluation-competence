<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class ImageConverter
 */
class ImageConverter
{
    /**
     * @param string $url
     *
     * @return UploadedFile
     */
    public function downloadImage(string $url): UploadedFile
    {
        $file = __DIR__ . "/../../../web/uploads/img/tmp.jpeg";

        file_put_contents($file, file_get_contents($url));

        //In order to create an UploadedFile without going through a form, I need to get to the constructor's last
        //parameter and set it to true
        $fileInfo = new \SplFileInfo($file);
        $mimeTypeGuesser = MimeTypeGuesser::getInstance();

        $file = new UploadedFile(
            $fileInfo->getRealPath(),
            $fileInfo->getBasename(),
            $mimeTypeGuesser->guess($fileInfo->getRealPath()),
            $fileInfo->getSize(),
            null,
            true
        );

        return $file;
    }
}
