<?php

namespace AppBundle\Service;

/**
 * Class SerieSearcher
 */
class SerieSearcher
{
    /**
     * Search the name of the serie in the output from the API, in both ['title'] and ['aliases']
     *
     * @param string $name
     * @param string[] $output
     *
     * @return array
     */
    public function search(string $name, array $output): array
    {
        for ($i = 0; $i < count($output); $i++) {
            if (strcasecmp($name, $output[$i]['title']) === 0) {
                return $output[$i];
            }

            for ($j = 0; $j < count($output[$i]['aliases']); $j++) {
                if (strcasecmp($name, $output[$i]['aliases'][$j]) === 0) {
                    return $output[$i];
                }
            }
        }

        return ['Not found'];
    }
}
