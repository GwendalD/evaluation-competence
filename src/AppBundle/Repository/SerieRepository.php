<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Serie;

/**
 * SerieRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SerieRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param int $limit
     *
     * @return Serie[]
     */
    public function mostWatched(int $limit): ?array
    {
        return $this->_em->createQuery(
            'SELECT s FROM AppBundle:Serie s ORDER BY s.consulted DESC'
        )
            ->setMaxResults($limit)
            ->getResult();
    }
}
