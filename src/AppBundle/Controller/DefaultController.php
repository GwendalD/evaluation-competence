<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Serie;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(): Response
    {
        return $this->render('default/layout.html.twig');
    }

    /**
     * @Route("/search")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function searchAction(Request $request, EntityManagerInterface $em): Response
    {
        $name = $request->request->get('name');

        $serie = $em->getRepository(Serie::class)->findOneBy(['name' => $name]);

        if (!$serie) {
            return $this->render('default/not_found.html.twig', [
                'name' => $name,
            ]);
        }

        return $this->redirectToRoute('app_serie_serie', [
            'name' => $serie->getName(),
        ]);
    }
}
