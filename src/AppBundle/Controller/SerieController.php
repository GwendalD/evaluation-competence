<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Episode;
use AppBundle\Entity\Serie;
use AppBundle\Manager\SerieManager;
use AppBundle\Service\BetaSeriesAPI;
use AppBundle\Service\SerieSearcher;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SerieController
 */
class SerieController extends Controller
{
    /**
     * @Route("/serie/{name}")
     *
     * @param Serie $serie
     * @param SerieManager $serieManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function serieAction(Serie $serie, SerieManager $serieManager): Response
    {
        $serieManager->updateSerie($serie);

        $seasons = $serie->getSeasons();

        return $this->render('serie/serie.html.twig', [
            'serie' => $serie,
            'seasons' => $seasons,
        ]);
    }

    /**
     * @Route("/serie/{name}/season_{season}")
     *
     * @param Serie $serie
     * @param int $season
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function seasonAction(Serie $serie, int $season, EntityManagerInterface $em): Response
    {
        $episodes = $em->getRepository(Episode::class)
            ->findBy(['serie' => $serie->getId(), 'season' => $season]);

        return $this->render('serie/season.html.twig', [
            'serie' => $serie,
            'season' => $season,
            'episodes' => $episodes,
        ]);
    }

    /**
     * @Route("/serie/{name}/season_{season}/episode_{number}")
     *
     * @param Serie $serie
     * @param int $season
     * @param int $number
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function episodeAction(Serie $serie, int $season, int $number, EntityManagerInterface $em): Response
    {
        $episode = $em->getRepository(Episode::class)
            ->findOneBy([
                'serie' => $serie->getId(),
                'season' => $season,
                'number' => $number,
            ]);

        $countEpisodes = count($em->getRepository(Episode::class)
            ->findBy(['serie' => $serie->getId(), 'season' => $season]));

        return $this->render('serie/episode.html.twig', [
            'serie' => $serie,
            'episode' => $episode,
            'countEpisodes' => $countEpisodes,
        ]);
    }

    /**
     * @param int $limit
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function mostWatchedAction(int $limit, EntityManagerInterface $em): Response
    {
        $series = $em->getRepository(Serie::class)->mostWatched($limit);

        return $this->render('serie/most_watched.html.twig', [
            'series' => $series,
        ]);
    }

    /**
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @Route("/add")
     *
     * @param Request $request
     * @param BetaSeriesAPI $betaSeriesAPI
     * @param SerieSearcher $serieSearcher
     * @param SerieManager $serieManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addSerieFromAPIAction(Request $request, BetaSeriesAPI $betaSeriesAPI, SerieSearcher $serieSearcher, SerieManager $serieManager): Response
    {
        if ($request->isMethod('POST')) {
            $name = $request->request->get('name');

            $output = $betaSeriesAPI->getSerieFromName($name);

            if (empty($output['shows'])) {
                $this->addFlash('info', 'Vérifiez l\'orthographe de la série demandée.');

                return $this->render('serie/add_serie.html.twig');
            }

            $data = $serieSearcher->search($name, $output['shows']);
            $serie = $serieManager->create($data);

            return $this->redirectToRoute('app_serie_serie', [
                'name' => $serie->getName(),
            ]);
        }

        return $this->render('serie/add_serie.html.twig');
    }

    /**
     * @Route("/admin/serie/")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function serieFromAdminAction(Request $request, EntityManagerInterface $em): Response
    {
        $id = $request->query->get('id');

        $serie = $em->getRepository(Serie::class)->find($id);

        $seasons = $serie->getSeasons();

        return $this->render('serie/serie.html.twig', [
            'serie' => $serie,
            'seasons' => $seasons,
        ]);
    }

    /**
     * @Route("/admin/episode/")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function episodeFromAdminAction(Request $request, EntityManagerInterface $em): Response
    {
        $id = $request->query->get('id');

        $episode = $em->getRepository(Episode::class)->find($id);

        return $this->render('serie/episode.html.twig', [
            'serie' => $episode->getSerie(),
            'episode' => $episode,
        ]);
    }
}
