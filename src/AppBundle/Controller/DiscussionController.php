<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Form\MessageType;
use AppBundle\Entity\Episode;
use AppBundle\Manager\DiscussionManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;

/**
 * Class DiscussionController
 */
class DiscussionController extends Controller
{
    /**
     * @param Episode $episode
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function displayMessagesAction(Episode $episode, EntityManagerInterface $em): Response
    {
        $messages = $em->getRepository(Message::class)
            ->findBy(['episode' => $episode]);

        return $this->render('discussion/messages.html.twig', [
            'messages' => $messages,
        ]);
    }

    /**
     * @param int $limit
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function lastMessagesAction(int $limit, EntityManagerInterface $em): Response
    {
        $messages = $em->getRepository(Message::class)->lastMessages($limit);

        return $this->render('discussion/last_messages.html.twig', [
            'messages' => $messages
        ]);
    }

    /**
     * @Route("/message/post/{id}")
     *
     * @param Request $request
     * @param Episode $episode
     * @param DiscussionManager $discussionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postMessageAction(Request $request, Episode $episode, DiscussionManager $discussionManager): Response
    {
        $form = $this->createForm(MessageType::class, null, [
            'action' => $this->generateUrl('app_discussion_postmessage', [
                'request' => $request,
                'id' => $episode->getId(),
            ])
        ]);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $discussionManager->postMessage($form->getData(), $episode, $this->getUser());

            return $this->redirectToRoute('app_serie_episode', [
                'name' => $episode->getSerie()->getName(),
                'season' => $episode->getSeason(),
                'number' => $episode->getNumber(),
            ]);
        }

        return $this->render('discussion/post_message.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/message/delete/{id}")
     *
     * @param Message $message
     * @param DiscussionManager $discussionManager
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteMessageAction(Message $message, DiscussionManager $discussionManager): Response
    {
        if ($this->isAllowed($message, $this->getUser())) {
            $discussionManager->deleteMessage($message);

            $this->addFlash('info', 'Message supprimé');
        } else {
            $this->addFlash('info', 'Vous ne pouvez supprimer que vos messages');
        }
        return $this->redirectToRoute('app_serie_episode', [
            'name' => $message->getEpisode()->getSerie()->getName(),
            'season' => $message->getEpisode()->getSeason(),
            'number' => $message->getEpisode()->getNumber(),
        ]);
    }

    /**
     * @Route("/message/edit/{id}")
     *
     * @param Request $request
     * @param Message $message
     * @param DiscussionManager $discussionManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editMessageAction(Request $request, Message $message, DiscussionManager $discussionManager): Response
    {
        if ($this->isAllowed($message, $this->getUser())) {
            $form = $this->createForm(MessageType::class, $message);

            if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
                $discussionManager->editMessage($message);

                $this->addFlash('info', 'Message édité');

                return $this->redirectToRoute('app_serie_episode', [
                    'name' => $message->getEpisode()->getSerie()->getName(),
                    'season' => $message->getEpisode()->getSeason(),
                    'number' => $message->getEpisode()->getNumber(),
                ]);
            }

            return $this->render('discussion/edit_message.html.twig', [
                'form' => $form->createView(),
            ]);
        }

        $this->addFlash('info', 'Vous ne pouvez éditer que vos messages');

        return $this->redirectToRoute('app_serie_episode', [
            'name' => $message->getEpisode()->getSerie()->getName(),
            'season' => $message->getEpisode()->getSeason(),
            'number' => $message->getEpisode()->getNumber(),
        ]);
    }

    /**
     * Check if the user is the message's owner or an admin
     *
     * @param Message $message
     * @param User $user
     *
     * @return bool
     */
    private function isAllowed(Message $message, User $user): bool
    {
        return (($user->getId() === $message->getUser()->getId()) || in_array('ROLE_ADMIN', $user->getRoles()));
    }

    /**
     * @Route("/admin/message")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function messageFromAdminAction(Request $request, EntityManagerInterface $em): Response
    {
        $id = $request->query->get('id');

        $message = $em->getRepository(Message::class)->find($id);

        return $this->redirectToRoute('app_serie_episodefromadmin', [
            'id' => $message->getEpisode()->getId(),
        ]);
    }
}
