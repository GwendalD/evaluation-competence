<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\EditPasswordType;
use AppBundle\Form\EditUserType;
use AppBundle\Form\UserType;
use AppBundle\Manager\UserManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class UserController
 */
class UserController extends Controller
{
    /**
     * @Route("/login")
     *
     * @param AuthenticationUtils $helper
     *
     * @return Response
     */
    public function loginAction(AuthenticationUtils $helper): Response
    {
        return $this->render('user/login.html.twig', [
            'last_username' => $helper->getLastUsername(),
            'error' => $helper->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/logout")
     *
     * @throws \Exception
     */
    public function logoutAction(): void
    {
        throw new \Exception('This should never be reached');
    }

    /**
     * @Route("/register")
     *
     * @param Request $request
     * @param UserManager $userManager
     *
     * @return Response
     */
    public function registerAction(Request $request, UserManager $userManager): Response
    {
        $form = $this->createForm(UserType::class, null);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $userManager->createUser($form->getData());

            return $this->redirectToRoute('app_user_login');
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @Route("/profile/{id}")
     *
     * @param User $user
     *
     * @return Response
     */
    public function profileAction(User $user): Response
    {
        return $this->render('user/profile.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @Route("/profile/edit/{id}")
     *
     * @param Request $request
     * @param User $user
     * @param UserManager $userManager
     *
     * @return Response
     */
    public function editAction(Request $request, User $user, UserManager $userManager): Response
    {
        $form = $this->createForm(EditUserType::class, $user);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $userManager->editUser($user);

            $this->addFlash('info', 'Informations éditées');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @Route("/profile/edit/password/{id}")
     *
     * @param Request $request
     * @param User $user
     * @param UserManager $userManager
     *
     * @return Response
     */
    public function editPassword(Request $request, User $user, UserManager $userManager): Response
    {
        $form = $this->createForm(EditPasswordType::class, $user);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $userManager->editPassword($user);

            $this->addFlash('info', 'Mot de passe édité');
        }

        return $this->render('user/edit_password.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
}
