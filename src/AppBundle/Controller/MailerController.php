<?php

namespace AppBundle\Controller;

use AppBundle\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\User;

/**
 * Class MailerController
 */
class MailerController extends Controller
{
    /**
     * @Route(path="/admin/send/user")
     *
     * @param Request $request
     * @param Mailer $mailer
     * @param EntityManagerInterface $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sendMailToUserAction(Request $request, Mailer $mailer, EntityManagerInterface $em): Response
    {
        $user = $em->getRepository(User::class)->find($request->query->get('id'));

        $datas = [
            'subject' => 'Test',
            'from' => $this->getParameter('mailer_user'),
            'to' => $user->getEmail(),
            'body' => 'Test Body',
        ];

        $mailer->sendMail($datas);

        $this->addFlash('info', 'E-mail envoyé');

        return $this->redirectToRoute('easyadmin', [
            'action' => 'list',
            'entity' => $request->query->get('entity'),
        ]);
    }
}
