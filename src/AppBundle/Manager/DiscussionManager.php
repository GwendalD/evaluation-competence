<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Episode;
use AppBundle\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\User;

/**
 * Class DiscussionManager
 */
class DiscussionManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DiscussionManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Message $message
     * @param Episode $episode
     * @param User $user
     *
     * @return Message
     */
    public function postMessage(Message $message, Episode $episode, User $user): Message
    {
        $message->setEpisode($episode);
        $message->setUser($user);
        $message->setDate(new \DateTime());

        $this->em->persist($message);
        $this->em->flush();

        return $message;
    }

    /**
     * @param Message $message
     */
    public function deleteMessage(Message $message): void
    {
        $this->em->remove($message);
        $this->em->flush();
    }

    /**
     * @param Message $message
     *
     * @return Message
     */
    public function editMessage(Message $message): Message
    {
        $this->em->persist($message);
        $this->em->flush();

        return $message;
    }
}
