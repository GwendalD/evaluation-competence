<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Episode;
use AppBundle\Entity\Serie;
use AppBundle\Service\BetaSeriesAPI;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class EpisodeManager
 */
class EpisodeManager
{
    /**
     * @var Session
     */
    private $session;

    /**
     * @var BetaSeriesAPI
     */
    private $betaSeriesAPI;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * EpisodeManager constructor.
     *
     * @param SessionInterface $session
     * @param BetaSeriesAPI $betaSeriesAPI
     * @param EntityManagerInterface $em
     */
    public function __construct(SessionInterface $session, BetaSeriesAPI $betaSeriesAPI, EntityManagerInterface $em)
    {
        $this->session = $session;
        $this->betaSeriesAPI = $betaSeriesAPI;
        $this->em = $em;
    }

    /**
     * @param Serie $serie
     * @param int[] $seasonsDetails
     *
     * @return Episode[]
     */
    public function createEpisodesFromAPI(Serie $serie, array $seasonsDetails): array
    {
        $numbers = $this->getEpisodesNumber($seasonsDetails);
        $episodes = [];

        foreach ($numbers as $number) {
            $output = $this->betaSeriesAPI->getEpisode($number, $serie);

            if (count($output['errors']) === 0) {
                $episodes[] = $this->createEpisode($serie, $output['episode']);
            } else {
                $this->session->getFlashBag()->add('warning', 'L\'épisode '.$number.' n\'existe pas');
            }
        }

        return $episodes;
    }

    /**
     * @param int[] $seasonsDetails
     *
     * @return array
     */
    public function getEpisodesNumber(array $seasonsDetails): array
    {
        $numbers = [];
        for ($seasonNumber = 0; $seasonNumber < count($seasonsDetails); $seasonNumber++) {
            for ($episodeNumber = 1; $episodeNumber <= $seasonsDetails[$seasonNumber]['episodes']; $episodeNumber++) {
                $numbers[] = 's' . $seasonsDetails[$seasonNumber]['number'] . 'e' . $episodeNumber;
            }
        }

        return $numbers;
    }

    /**
     * @param Serie $serie
     * @param string[] $output
     *
     * @return Episode
     */
    public function createEpisode(Serie $serie, array $output): Episode
    {
        $episode  = new Episode();
        $episode->setTitle($output['title'])
            ->setSynopsys($output['description'])
            ->setSerie($serie)
            ->setSeason($output['season'])
            ->setNumber($output['episode']);

        $serie->addEpisode($episode);

        $this->em->persist($episode);

        return $episode;
    }
}
