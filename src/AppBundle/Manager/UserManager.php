<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Service\PasswordEncoder;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserManager
 */
class UserManager
{
    /**
     * @var PasswordEncoder
     */
    private $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * UserManager constructor.
     *
     * @param PasswordEncoder $passwordEncoder
     * @param EntityManagerInterface $em
     */
    public function __construct(PasswordEncoder $passwordEncoder, EntityManagerInterface $em)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function createUser(User $user): User
    {
        $password = $this->passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($password)
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $pwd
     *
     * @return User
     */
    public function createUserFromCommand(string $username, string $email, string $pwd): User
    {
        $user = new User();
        $password = $this->passwordEncoder->encodePassword($user, $pwd);
        $user->setUsername($username)
            ->setEmail($email)
            ->setPassword($password)
            ->setRoles(['ROLE_USER'])
            ->setEnabled(true);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param string $role
     *
     * @return User
     */
    public function changeRoleFromCommand(User $user, string $role): User
    {
        $user->setRoles([$role]);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function editUser(User $user): User
    {
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function editPassword(User $user): User
    {
        $password = $this->passwordEncoder->encodePassword($user, $user->getPassword());

        $user->setPassword($password);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }
}
