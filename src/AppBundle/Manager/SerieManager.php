<?php

namespace AppBundle\Manager;

use AppBundle\Service\SerieSynchronizer;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Serie;
use AppBundle\Service\BetaSeriesAPI;
use AppBundle\Service\ImageConverter;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class SerieManager
 */
class SerieManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ImageConverter
     */
    private $imageConverter;

    /**
     * @var BetaSeriesAPI
     */
    private $betaSeriesAPI;

    /**
     * @var EpisodeManager
     */

    private $episodeManager;
    /**
     * @var SerieSynchronizer
     */
    private $serieSynchronizer;
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * SerieManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param ImageConverter $imageConverter
     * @param BetaSeriesAPI $betaSeriesAPI
     * @param EpisodeManager $episodeManager
     * @param SerieSynchronizer $serieSynchronizer
     */
    public function __construct(
        EntityManagerInterface $em,
        ImageConverter $imageConverter,
        BetaSeriesAPI $betaSeriesAPI,
        EpisodeManager $episodeManager,
        SerieSynchronizer $serieSynchronizer,
        RouterInterface $router
    ) {
        $this->em = $em;
        $this->imageConverter = $imageConverter;
        $this->betaSeriesAPI = $betaSeriesAPI;
        $this->episodeManager = $episodeManager;
        $this->serieSynchronizer = $serieSynchronizer;
        $this->router = $router;
    }

    /**
     * @param string[] $output
     *
     * @return Serie
     */
    public function create(array $output): Serie
    {
        $serie = new Serie();
        $serie->setBetaId($output['id'])
            ->setName($output['title'])
            ->setNbSeasons($output['seasons'])
            ->setSynopsys($output['description'])
            ->setUpdatedAt(new \DateTime())
            ->setImageFile($this->imageConverter->downloadImage($output['images']['show']))
            ->setConsulted(0);

        $this->episodeManager->createEpisodesFromAPI($serie, $output['seasons_details']);

        $this->em->persist($serie);
        $this->em->flush();

        return $serie;
    }

    /**
     * @param Serie $serie
     */
    public function updateSerie(Serie $serie): void
    {
        $serie->setConsulted($serie->getConsulted() + 1);

        $this->em->persist($serie);
        $this->em->flush();
    }
}
