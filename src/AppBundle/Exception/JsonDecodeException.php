<?php

namespace AppBundle\Exception;


/**
 * Class JsonDecodeException
 */
class JsonDecodeException extends \RuntimeException
{
}