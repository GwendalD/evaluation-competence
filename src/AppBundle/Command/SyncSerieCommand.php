<?php

namespace AppBundle\Command;

use AppBundle\Entity\Episode;
use AppBundle\Entity\Serie;
use AppBundle\Service\SerieSynchronizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncSerieCommand
 */
class SyncSerieCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SerieSynchronizer
     */
    private $synchronizer;

    public function __construct(EntityManagerInterface $em, SerieSynchronizer $synchronizer)
    {
        $this->em = $em;
        $this->synchronizer = $synchronizer;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('serie:sync')
            ->setDescription('Synchronise all series with BetaSeries')
            ->setHelp('Synchronise all series with BetaSeries');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $series = $this->em->getRepository(Serie::class)->findAll();

        foreach ($series as $serie) {
            $this->synchronizer->syncSerie($serie);


            $episodes = $this->em->getRepository(Episode::class)->findBy(['serie' => $serie->getId()]);

            $this->synchronizer->updateEpisodes($serie, $episodes);
        }

        $output->writeln('Series synchronized');
    }
}
