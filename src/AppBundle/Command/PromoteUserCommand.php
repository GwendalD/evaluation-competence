<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use AppBundle\Manager\UserManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PromoteUserCommand
 */
class PromoteUserCommand extends Command
{
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(UserManager $userManager, EntityManagerInterface $em)
    {
        parent::__construct();
        $this->userManager = $userManager;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('user:promote')
            ->setDescription('Promotes a user')
            ->setHelp('Promotes a user')
            ->addArgument('user', InputArgument::REQUIRED)
            ->addArgument('role', InputArgument::REQUIRED);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('user');
        $role = $input->getArgument('role');

        $user = $this->em->getRepository(User::class)->findOneBy([
            'username' => $username
        ]);

        $this->userManager->changeRoleFromCommand($user, $role);

        $output->writeln(sprintf('Promoted user %s', $username));
    }
}
