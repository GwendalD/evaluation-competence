<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Entity\File as EmbeddedFile;

/**
 * Serie
 *
 * @ORM\Table(name="serie")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SerieRepository")
 * @Vich\Uploadable
 */
class Serie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="betaId", type="integer")
     */
    private $betaId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_seasons", type="integer")
     */
    private $nbSeasons;

    /**
     * @var Collection
     *
     * One Serie has Many Episodes
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Episode", mappedBy="serie",cascade={"persist", "remove"})
     */
    private $episodes;

    /**
     * @var int
     *
     * @ORM\Column(name="consulted", type="integer")
     */
    private $consulted;

    /**
     * @var string
     *
     * @ORM\Column(name="synopsys", type="text", nullable=true)
     */
    private $synopsys;

    /**
     *
     * @Vich\UploadableField(mapping="serie_image", fileNameProperty="image.name", size="image.size", mimeType="image.mimeType", originalName="image.originalName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Embedded(class="Vich\UploaderBundle\Entity\File")
     *
     * @var EmbeddedFile
     */
    private $image;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->episodes = new ArrayCollection();
        $this->image = new EmbeddedFile();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Serie
     */
    public function setName($name): Serie
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set nbSeasons
     *
     * @param integer $nbSeasons
     *
     * @return Serie
     */
    public function setNbSeasons($nbSeasons): Serie
    {
        $this->nbSeasons = $nbSeasons;

        return $this;
    }

    /**
     * Get nbSeasons
     *
     * @return int
     */
    public function getNbSeasons(): int
    {
        return $this->nbSeasons;
    }

    /**
     * Set synopsys
     *
     * @param string $synopsys
     *
     * @return Serie
     */
    public function setSynopsys($synopsys): Serie
    {
        $this->synopsys = $synopsys;

        return $this;
    }

    /**
     * Get synopsys
     *
     * @return string
     */
    public function getSynopsys(): string
    {
        return $this->synopsys;
    }

    /**
     * Set consulted
     *
     * @param integer $consulted
     *
     * @return Serie
     */
    public function setConsulted($consulted): Serie
    {
        $this->consulted = $consulted;

        return $this;
    }

    /**
     * Get consulted
     *
     * @return integer
     */
    public function getConsulted(): int
    {
        return $this->consulted;
    }

    /**
     *
     * @param File|UploadedFile $image
     *
     * @return Serie
     */
    public function setImageFile(File $image = null): Serie
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param EmbeddedFile $image
     *
     * @return Serie
     */
    public function setImage(EmbeddedFile $image): Serie
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return EmbeddedFile
     */
    public function getImage(): EmbeddedFile
    {
        return $this->image;
    }

    /**
     * @param int $betaId
     *
     * @return Serie
     */
    public function setBetaId(int $betaId): Serie
    {
        $this->betaId = $betaId;

        return $this;
    }

    /**
     * @return int
     */
    public function getBetaId(): int
    {
        return $this->betaId;
    }

    /**
     * Get seasons
     *
     * @return int[]
     *
     * returns an array of integers corresponding to each seasons in the serie
     * in order to build links in the  serie view
     */
    public function getSeasons(): array
    {
        $seasons = [];

        for ($i = 1; $i <= $this->nbSeasons; $i++) {
            $seasons[] = $i;
        }

        return $seasons;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Serie
     */
    public function setUpdatedAt($updatedAt): Serie
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Add episode.
     *
     * @param \AppBundle\Entity\Episode $episode
     *
     * @return Serie
     */
    public function addEpisode(\AppBundle\Entity\Episode $episode): Serie
    {
        $this->episodes[] = $episode;

        return $this;
    }

    /**
     * Remove episode.
     *
     * @param \AppBundle\Entity\Episode $episode
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeEpisode(\AppBundle\Entity\Episode $episode): bool
    {
        return $this->episodes->removeElement($episode);
    }

    /**
     * Get episodes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEpisodes(): Collection
    {
        return $this->episodes;
    }
}
