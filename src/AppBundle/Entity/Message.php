<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Discussion
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"})
     */
    private $user;

    /**
     * @var Episode
     *
     * Many Messages have One Episoed
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Episode", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $episode;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     * @Assert\NotBlank(message="Veuillez écrire un message")
     * @Assert\Length(
     *     min = 10,
     *     minMessage="Votre message doit contenir au minimum {{ limit }} charactères"
     * )
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return Message
     */
    public function setContent($content): Message
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Message
     */
    public function setDate($date): Message
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * Set user.
     *
     * @param User $user
     *
     * @return Message
     */
    public function setUser(User $user = null): Message
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Set episode.
     *
     * @param \AppBundle\Entity\Episode|null $episode
     *
     * @return Message
     */
    public function setEpisode(\AppBundle\Entity\Episode $episode = null): Message
    {
        $this->episode = $episode;

        return $this;
    }

    /**
     * Get episode.
     *
     * @return Episode
     */
    public function getEpisode(): Episode
    {
        return $this->episode;
    }
}
